<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ServiceController;


Route::get('/', [ServiceController::class, 'productsPage'])->name('products');
Route::get('/create', [ServiceController::class, 'productCreatePage'])->name('product.create');
Route::get('/change-product/{id}', [ServiceController::class, 'productChangePage']);


Route::get('/delete-product/{id}', [ServiceController::class, 'productDeleteAction']);
Route::post('/add_product', [ServiceController::class, 'productCreateAction'])->name('action.product.create');
Route::post('/change_product', [ServiceController::class, 'productChangeAction'])->name('action.product.change');
