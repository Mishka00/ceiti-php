@section('header')
    <header>
        <nav class="navbar navbar-light bg-light  navbar-expand-lg justify-content-between py-2 px-lg-5">
            <div></div>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{ request()->routeIs('products') ? 'active font-weight-bold' : '' }}">
                        <a class="nav-link" href="{{ route('products') }}">Arata toate produsele</a>
                    </li>
                    <li class="nav-item {{ request()->routeIs('product.create') ? 'active font-weight-bold' : '' }}">
                        <a class="nav-link" href="{{ route('product.create') }}">Creaza produs</a>
                    </li>
                </ul>
            </div>
            <div></div>
        </nav>
    </header>
    <style>
        .header__cart {
            position: relative;
        }

        .header__cart--count {
            top: -6px;
            right: -2px;
            position: absolute;
            transform: translate(50%, -50%);
            min-width: 35px;
        }

    </style>
