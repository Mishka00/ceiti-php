<a href="/change-product/{{ $product->id }}">
<div class="m-3 card shadow rounded border border-1">
    <div style="min-height: 150px">
        <img src="{{ $product->photo_url }}" class="card-img-top">
    </div>
    <div class="card-body text-center">
        <h5 class="card-title">{{ $product->title }}</h5>
        <span class="fw-bold">{{ $product->price }} lei</span>
        <div class="border p-2 mt-2 card-text">
            {{ $product->information }}
        </div>
    </div>
</div>
 </a>
