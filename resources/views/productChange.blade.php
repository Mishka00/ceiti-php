@extends('layouts.main')

@section('title') Welcome @endsection

@section('content')
    @if (isset($data['product']))
        @foreach ($data['product'] as $product)
            <form class="mt-50" method="post" action="{{ route('action.product.change') }}">
                @csrf
                <h1 class="text-center p-5">Schimba produs</h1>
                <div>
                    <div class="row p-4">
                        <input style="display: none" type="number" value="{{ $product->id }}" name="id">
                        <div class="col-md-12 p-2">
                            <label for="name" class="mb-2">Numele</label>
                            <input type="text" value="{{ $product->name }}" class="p-1 form-control" id="name"
                                name="name">
                        </div>
                        <div class="col-md-12 p-2">
                            <label for="title" class="mb-2">Titlul prudusului</label>
                            <input type="text" value="{{ $product->title }}" class="p-1 form-control" id="title"
                                name="title">
                        </div>
                        <div class="col-md-12 p-2">
                            <label for="photo_url" class="mb-2 null">Linkul la produs</label>
                            <input type="text" value="{{ $product->photo_url }}" class="p-1 form-control" id="photo_url"
                            name="photo_url">
                        </div>
                        <div class="col-md-12 p-2">
                            <label for="price" class="mb-2 null">Pretul produsului</label>
                            <input type="number" value="{{ $product->price }}" class="p-1 form-control" id="price"
                                name="price">
                        </div>
                        <div class="col-md-12 p-2">
                            <label for="description" class="mb-2 null">Informatia</label>
                            <input required type="text" value="{{ $product->information }}" class="p-1 form-control"
                                id="information" name="information">
                        </div>
                        <div class="col-md-12 p-2">
                            <label for="description" class="mb-2 null">Descrierea</label>
                            <input required type="text" value="{{ $product->description }}" class="p-1 form-control"
                                id="description" name="description">
                        </div>
                        <div class="col-md-12 p-2 text-center mt-5">
                            <button type="submit" class="btn m-3 btn-primary">Salveaza schimbarile</button>
                        </div>
                    </div>
                </div>
            </form>
        @endforeach
    @endif
@endsection
