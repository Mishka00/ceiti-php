@extends('layouts.main')

@section('title') Welcome @endsection

@section('content')
    <h1 class="text-center p-5">Products</h1>
    <div class="container">
        <div class="products">
            @if (isset($data['products']))
                @foreach ($data['products'] as $product)
                    @include('productsPreview')
                @endforeach
            @endif
        </div>
    </div>
    <style>
        .products {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between
        }

        .products>.card {
            width: 320px
        }

        .noWrap {
            white-space: nowrap
        }

    </style>
@endsection
