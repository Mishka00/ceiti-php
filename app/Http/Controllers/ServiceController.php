<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProductChangeRequest;
use App\Http\Requests\ProductCreateRequest;
use Illuminate\Support\Facades\Redirect;

class ServiceController extends Controller
{

    public function productsPage() {

        $data = ['products' => DB::table('products')->get()];

        return view('products')->with(['data' =>  $data]);
    }

    public function productCreatePage() {

        return view('productCreate');
    }

    public function productChangePage($id) {

        $data = [
            'product' => DB::table('products')->where('id', $id)->get(),
        ];

        if ($data['product']) {
            return view('productChange')->with(['data' =>  $data]);
        } else {
            return view('products');
        }
    }

    public function productDeleteAction($id) {
        $product = DB::table('products')->where('id', $id)->get();

        if ($product) {
            DB::table('products')->where('id', $id)->delete();
        }

        return  Redirect('');
    }


    public function productCreateAction(ProductCreateRequest $req)
    {
        $product = $req->all();
        $insert = [];

        if (($product['name'])) {
            $insert['name'] = $product['name'];
        }
        if (($product['title'])) {
            $insert['title'] = $product['title'];
        }
        if (($product['description'])) {
            $insert['description'] = $product['description'];
        }
        if (($product['information'])) {
            $insert['information'] = $product['information'];
        }
        if (($product['price'])) {
            $insert['price'] = $product['price'];
        }
        if (($product['photo_url'])) {
            $insert['photo_url'] = $product['photo_url'];
        }

        DB::table('products')->insert($insert);

        return Redirect::route('products');
    }


    public function productChangeAction(ProductChangeRequest $req)
    {
        $product = $req->all();
        $insert = [];


        if (($product['name'])) {
            $insert['name'] = $product['name'];
        }
        if (($product['title'])) {
            $insert['title'] = $product['title'];
        }
        if (($product['description'])) {
            $insert['description'] = $product['description'];
        }
        if (($product['information'])) {
            $insert['information'] = $product['information'];
        }
        if (($product['price'])) {
            $insert['price'] = $product['price'];
        }
        if (($product['photo_url'])) {
            $insert['photo_url'] = $product['photo_url'];
        }

        DB::table('products')->where('id', $product['id'])->update($insert);


        return Redirect::route('products');
    }
}
